// "use-strict";

// const _ = require("lodash");

// const { has, pipe, prop, pick } = require("lodash/fp");
// // const { setCreatorFields } = require("strapi-utils");

// const {
//   getService,
//   wrapBadRequest,
//   pickWritableAttributes,
// } = require("../utils");

// module.exports = {
//   // async update(ctx) {
//   //   const { id, model } = ctx.params;

//   //   console.log("STRAPI");

//   //   const contentManagerService =
//   //     strapi.plugins["content-manager"].services.contentmanager;

//   //   try {
//   //     if (ctx.is("multipart")) {
//   //       const { data, files } = parseMultipartBody(ctx);
//   //       ctx.body = await contentManagerService.edit({ id }, data, {
//   //         files,
//   //         model,
//   //       });
//   //     } else {
//   //       // Return the last one which is the current model.
//   //       ctx.body = await contentManagerService.edit({ id }, ctx.request.body, {
//   //         model,
//   //       });
//   //     }
//   //   } catch (error) {
//   //     strapi.log.error(error);
//   //     ctx.badRequest(null, [
//   //       {
//   //         message: [
//   //           { id: error.message, message: error.message, field: error.field },
//   //         ],
//   //         errors: _.get(error, "data.errors"),
//   //       },
//   //     ]);
//   //   }
//   // },

//   async update(ctx) {
//     const { userAbility, user } = ctx.state;
//     const { id, model } = ctx.params;
//     const { body } = ctx.request;
//     console.log("STRAPI");
//     const entityManager = getService("entity-manager");
//     const permissionChecker = getService("permission-checker").create({
//       userAbility,
//       model,
//     });

//     if (permissionChecker.cannot.update()) {
//       return ctx.forbidden();
//     }

//     const entity = await entityManager.findOneWithCreatorRoles(id, model);

//     if (!entity) {
//       return ctx.notFound();
//     }

//     if (permissionChecker.cannot.update(entity)) {
//       return ctx.forbidden();
//     }

//     const pickWritables = pickWritableAttributes({ model });
//     const pickPermittedFields = permissionChecker.sanitizeUpdateInput(entity);
//     // const setCreator = setCreatorFields({ user, isEdition: true });

//     const sanitizeFn = pipe([pickWritables, pickPermittedFields]);

//     await wrapBadRequest(async () => {
//       const updatedEntity = await entityManager.update(
//         entity,
//         sanitizeFn(body),
//         model
//       );

//       ctx.body = permissionChecker.sanitizeOutput(updatedEntity);
//     })();
//   },
// };
