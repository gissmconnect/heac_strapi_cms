module.exports = {
  definition: `
    type AdvertiseDataReview {
     message : String
    }
    
  `,
  query: `review(id: Int!): AdvertiseDataReview`,
  resolver: {
    Query: {
      review: {
        description: "Return Advertise Info",
        resolverOf: "application::advertise.advertise.findOne",
        resolver: async (obj, options, ctx) => {
          let message = "Data Consoled Successfully!";
          const complianceInfo = await strapi
            .query("advertise")
            .findOne({ id: options.id });

          console.log("complianceInfo", complianceInfo);
          let responseObject = {
            message: message,
          };
          return responseObject;
        },
      },
    },
  },
};
