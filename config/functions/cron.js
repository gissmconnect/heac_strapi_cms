'use strict';
const moment = require('moment');

/**
 * Cron config that gives you an opportunity
 * to run scheduled jobs.
 *
 * The cron format consists of:
 * [SECOND (optional)] [MINUTE] [HOUR] [DAY OF MONTH] [MONTH OF YEAR] [DAY OF WEEK]
 *
 * See more details here: https://strapi.io/documentation/developer-docs/latest/setup-deployment-guides/configurations.html#cron-tasks
 */

module.exports = {
  '*/1 * * * *': async () => {
    console.log("cron has start",new Date());
    const updateTime = moment()
      .subtract(1000, 'minutes')
      .format('YYYY-MM-DD HH:mm:ss');

    // currentTime
    await strapi.elastic.migrateModels({
      conditions: {
        updated_at_gt: updateTime,
        /* to utilise Draft/Publish feature & migrate only published entities 
        you can add following in conditions
        */
        _publicationState: 'live'
      },
    });
  }
};

