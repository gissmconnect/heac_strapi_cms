module.exports = ({ env }) => ({
  defaultConnection: "default",
  connections: {
    default: {
      connector: "bookshelf",
      settings: {
        client: "postgres",
        database: "heaccms",
        host: "127.0.0.1",
        port: 5432,
        username: "postgres",
        password: "root",
      },
      options: {
        useNullAsDefault: true,
      },
    },
  },
});
