module.exports = ({ env }) => ({
  host: env('HOST', '0.0.0.0'),
  port: env.int('PORT', 1337),
  cron:{
    enabled:true
  },
  admin: {
    auth: {
      secret: env('ADMIN_JWT_SECRET', 'c7c0f1cd4c103db84aa977d6d8dc19ca'),
    },
  },
});
